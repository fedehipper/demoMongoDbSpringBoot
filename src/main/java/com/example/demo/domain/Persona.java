package com.example.demo.domain;

import java.math.BigInteger;
import org.springframework.data.annotation.Id;

public class Persona {

    @Id
    private BigInteger id;

    private String nombre;
    private String apellido;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

}
