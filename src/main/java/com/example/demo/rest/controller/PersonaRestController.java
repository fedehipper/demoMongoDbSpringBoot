package com.example.demo.rest.controller;

import com.example.demo.domain.Persona;
import com.example.demo.repository.PersonaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaRestController {

    @Autowired
    private PersonaRepository personaRepository;

    @GetMapping("/api/persona")
    public List<Persona> buscar() {
        return personaRepository.findAll();
    }

}
