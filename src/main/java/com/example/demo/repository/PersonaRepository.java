package com.example.demo.repository;

import com.example.demo.domain.Persona;
import java.math.BigInteger;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PersonaRepository extends MongoRepository<Persona, BigInteger> {

}
